//
//  main.cpp
//  CPPDAY2
//
//  Created by Jinglei Zhu on 06/12/2018.
//  Copyright © 2018 Jinglei Zhu. All rights reserved.
//08/12/2018 DAY2

#include <iostream>

int main(int argc, const char * argv[]) {
    int number_1=100;
    int number_2=200;
    std::cout<<"the sum of number 1 and number 2 is "<< number_1 + number_2 << std::endl;
    
    std::cout << "Hello, World!\n";
    std::cout<<"Please input two number"<<std::endl;
    
    int input_1, input_2;
    
    std::cin>>input_1>>input_2;
    
    std:: cout <<"The sum of" <<input_1<<" and " <<input_2
    <<" is "<< input_1 + input_2 <<std::endl;
    // simple plus function;
    
    int sum = 0, value=1;
    while(value<=9999){
        sum +=value;
        value +=2;
    }
    std::cout<<"the sum of 1 to 9999 jump 2 inclusive is " <<
    sum<<std::endl;
    
    //the while loop learning
    
    int sum_for =0;
    for (int val = 1; val <=100; ++val)
        sum_for +=val;
    std::cout<<"the test for the for loop "<<sum_for<<std::endl;
    // the for loop example
    
    return 0;
}
